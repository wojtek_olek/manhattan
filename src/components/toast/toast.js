import React, { Component } from "react";
import styles from "./toast.scss";

class toast extends Component {
  state = {
    visible: false
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.visible !== nextProps.visible) {
      this.setState({
        visible: nextProps.visible
      });
    }
  }

  render() {
    const classes = [styles.Toast];
    if (this.props.visible) {
      classes.push(styles.ToastVisible);
    }
    return (
      <div className={classes.join(" ")}>
        <p>{this.props.message}</p>
      </div>
    );
  }
}

export default toast;
