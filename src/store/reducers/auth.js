import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";
import { auth } from "../../Firebase";

const initialState = {
  isAuth: false,
  error: null,
  uId: null,
  currentUser: null,
  userLoading: true,
  toastMessage: null,
  toastVisible: false
};

const authSuccess = (state, action) => {
  const uid = auth.currentUser.uid;
  return updateObject(state, {
    isAuth: true,
    uId: uid,
    userLoading: true
  });
};

const authFail = (state, action) => {
  return updateObject(state, {
    error: action.error
  });
};

const authLogout = (state, action) => {
  return updateObject(state, { isAuth: false, userLoading: false });
};

const userData = (state, action) => {
  return updateObject(state, { currentUser: action.userData });
};

const userNotLoading = (state, action) => {
  return updateObject(state, { userLoading: false });
};

const updateSuccessful = (state, action) => {
  return updateObject(state, {
    toastMessage: "Save successful!",
    toastVisible: true
  });
};

const hideToast = (state, action) => {
  return updateObject(state, { toastVisible: false });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_FAIL:
      return authFail(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state, action);
    case actionTypes.GET_USER_DATA:
      return userData(state, action);
    case actionTypes.USER_NOT_LOADING:
      return userNotLoading(state, action);
    case actionTypes.UPDATE_SUCCESSFUL:
      return updateSuccessful(state, action);
    case actionTypes.HIDE_TOAST:
      return hideToast(state, action);
    default:
      return state;
  }
};

export default reducer;
