import * as actionTypes from "./actionTypes";
import { auth, database, storage } from "../../Firebase";

export const userNotLoading = () => {
  return {
    type: actionTypes.USER_NOT_LOADING
  }
}

export const authSuccess = isAuth => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    isAuth: isAuth
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

export const onLogout = () => {
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const logout = () => {
  return dispatch => {
    auth.signOut();
    dispatch(onLogout());
  };
};

export const login = (email, password) => {
  return dispatch => {
    auth.signInWithEmailAndPassword(email, password).then(response => {
      dispatch(authSuccess(true));
    });
  };
};

export const authentication = (user, selectedOption) => {
  const { firstName, lastName, email, password } = user;
  return dispatch => {
    if (selectedOption === "login") {
      dispatch(login(email, password));
    } else {
      auth
        .createUserWithEmailAndPassword(email, password)
        .then(user => {
          dispatch(authSuccess(true));
          if (user !== null) {
            database
              .ref("users")
              .child(user.uid)
              .set({
                firstName,
                lastName,
                email
              })
              .then(() => {
                dispatch(login(email, password));
              });
          }
        })
        .catch(error => {
          dispatch(authFail(error));
        });
    }
  };
};

export const authCheckState = authStatus => {
  return dispatch => {
    if (!authStatus) {
      dispatch(logout());
    } else {
      dispatch(authSuccess(true));
    }
  };
};

export const updateSuccessful = () => {
  return {
    type: actionTypes.UPDATE_SUCCESSFUL
  }
}

export const hideToast = () => {
  return {
    type: actionTypes.HIDE_TOAST
  }
}

export const updateUserData = userData => {
  return dispatch => {
    if (userData.userPhoto !== null) {
      storage
        .child(`userPhotos/${new Date().getTime()}`)
        .put(userData.userPhoto)
        .then(snapshot => {
          database
            .ref("users")
            .child(userData.uId)
            .set({
              firstName: userData.firstName,
              lastName: userData.lastName,
              city: userData.city,
              phone: userData.phone,
              image: snapshot.metadata.downloadURLs[0]
            })
            .then(() => {
              dispatch(updateSuccessful());
              setTimeout(() => {
                dispatch(hideToast())
              }, 3000);
            });
        });
    } else {
      database
            .ref("users")
            .child(userData.uId)
            .set({
              firstName: userData.firstName,
              lastName: userData.lastName,
              city: userData.city,
              phone: userData.phone,
              image: userData.userPhotoUrl
            })
            .then(() => {
              dispatch(updateSuccessful());
              setTimeout(() => {
                dispatch(hideToast())
              }, 3000);
            });
    }
  };
};

export const getUserData = userData => {
  return {
    type: actionTypes.GET_USER_DATA,
    userData: userData
  };
};

export const getUser = uId => {
  return dispatch => {
    database.ref(`users/${uId}`).on("value", db => {
      dispatch(getUserData(db.val()));
    });
  };
};
