export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAIL = "AUTH_FAIL";
export const AUTH_LOGOUT = "AUTH_LOGOUT";

export const GET_USER_DATA = "GET_USER_DATA";
export const USER_NOT_LOADING = "USER_NOT_LOADING";
export const UPDATE_SUCCESSFUL = "UPDATE_SUCCESSFUL";
export const HIDE_TOAST = "HIDE_TOAST";