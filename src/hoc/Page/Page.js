import React from 'react';
import Aux from '../Aux/Aux';
import styles from './Page.scss';

const page = props => (
  <Aux>
    <h4 className={styles.PageTitle}>
      {props.pageTitle}
    </h4>
    <div className={styles.Page}>
      {props.children}
    </div>
  </Aux>
)


export default page;