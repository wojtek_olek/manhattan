import React, { Component } from "react";
import styles from "./Layout.scss";
import { connect } from "react-redux";
import Aux from "../Aux/Aux";
import Sidebar from "../../containers/Sidebar/Sidebar";
import Toast from "../../components/toast/toast";

class Layout extends Component {
  render() {
    return (
      <Aux>
        {this.props.userLoading ? <Sidebar /> : null}
        <main className={styles.Content}>{this.props.children}</main>
        <Toast visible={this.props.toastVisible} message={this.props.toastMessage} />
      </Aux>
    );
  }
}

const mapState = state => {
  return {
    userLoading: state.auth.userLoading,
    toastMessage: state.auth.toastMessage,
    toastVisible: state.auth.toastVisible
  }
}

export default connect(mapState)(Layout);
