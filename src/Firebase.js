import * as firebase from "firebase";

const config = {
  apiKey: "AIzaSyDmCNklwEWL5sLeOAG83Hd3rb2XlBuRoyE",
  authDomain: "manhattan-b2dca.firebaseapp.com",
  databaseURL: "https://manhattan-b2dca.firebaseio.com",
  projectId: "manhattan-b2dca",
  storageBucket: "manhattan-b2dca.appspot.com",
  messagingSenderId: "927565609633"
};
firebase.initializeApp(config);

export const database = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage().ref();
