import React, { Component } from "react";
import { Route, withRouter, Switch } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "./store/actions/index";
import { auth } from "./Firebase";
// Components
import Layout from "./hoc/Layout/Layout";
import Home from "./containers/Home/Home";
import Auth from "./containers/Auth/Auth";
import Projects from "./containers/Projects/Projects";
import Preferences from "./containers/Preferences/Preferences";

class App extends Component {
  componentWillMount() {
    auth.onAuthStateChanged(user => {
      if (user) {
        this.props.onTryAutoSingup(true);
        this.props.getUserData(this.props.uId);
      } else {
        this.props.userNotLoading();
      }
    });
  }

  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/projects" component={Projects} />
          <Route path="/preferences" component={Preferences} />
          <Route path="/auth" component={Auth} />
        </Switch>
      </Layout>
    );
  }
}

const mapState = state => {
  return {
    uId: state.auth.uId,
    current: state.auth.currentUser
  };
};

const mapActions = dispatch => {
  return {
    onTryAutoSingup: authStatus => dispatch(actions.authCheckState(authStatus)),
    getUserData: uId => dispatch(actions.getUser(uId)),
    userNotLoading: () => dispatch(actions.userNotLoading())
  };
};

export default withRouter(connect(mapState, mapActions)(App));
