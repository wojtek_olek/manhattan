import React, { Component } from "react";
import Page from '../../hoc/Page/Page';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

class Home extends Component {
  render() {
    let authRedirect = null;
    if (!this.props.isAuth && !this.props.userLoading) {
      authRedirect = <Redirect to="/auth" />;
    }

    return (
      <Page pageTitle="Home">
        {authRedirect}
        Home working!
      </Page>
    );
  }
}

const mapState = state => {
  return {
    isAuth: state.auth.isAuth,
    userLoading: state.auth.userLoading
  };
};

export default connect(mapState)(Home);
