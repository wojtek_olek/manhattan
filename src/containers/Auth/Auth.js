import React, { Component } from "react";
import styles from "./Auth.scss";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Input from "../../components/UI/input/input";
import * as actions from "../../store/actions/index";

class Auth extends Component {
  state = {
    authForm: {
      firstName: {
        elementType: "input",
        label: "First Name",
        elementConfig: {
          type: "text"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      lastName: {
        elementType: "input",
        label: "Last Name",
        elementConfig: {
          type: "text"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      email: {
        elementType: "input",
        label: "E-mail",
        elementConfig: {
          type: "email"
        },
        value: "",
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
        touched: false
      },
      password: {
        elementType: "input",
        label: "Password",
        elementConfig: {
          type: "password"
        },
        value: "",
        validation: {
          required: true,
          minLength: 6
        },
        valid: false,
        touched: false
      }
    },
    selectedOption: "login"
  };

  checkValidity = (value, rules) => {
    let isValid = true;
    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    return isValid;
  };

  inputChangeHandler = (event, inputId) => {
    const upInputs = {
      ...this.state.authForm,
      [inputId]: {
        ...this.state.authForm[inputId],
        value: event.target.value,
        valid: this.checkValidity(
          event.target.value,
          this.state.authForm[inputId].validation
        ),
        touched: true
      }
    };
    this.setState({ authForm: upInputs });
  };

  radioCheckedHandler = option => {
    this.setState({ selectedOption: option });
  };

  onFormSubmitHandler = event => {
    event.preventDefault();
    let user = null;
    if (this.state.selectedOption === "login") {
      user = {
        email: this.state.authForm.email.value,
        password: this.state.authForm.password.value
      };
    } else if (this.state.selectedOption === "signup") {
      user = {
        email: this.state.authForm.email.value,
        password: this.state.authForm.password.value,
        firstName: this.state.authForm.firstName.value,
        lastName: this.state.authForm.lastName.value
      };
    }

    this.props.onAuth(user, this.state.selectedOption);
  };

  render() {
    let authRedirect = null;
    if (this.props.isAuth) {
      authRedirect = <Redirect to="/" />;
    }
    const formElements = [];
    for (let key in this.state.authForm) {
      formElements.push({
        id: key,
        config: this.state.authForm[key]
      });
    }

    const form = formElements.map(element => (
      <Input
        key={element.id}
        elementType={element.config.elementType}
        elementConfig={element.config.elementConfig}
        label={element.config.label}
        value={element.config.value}
        invalid={!element.config.valid}
        shouldValidate={element.config.validation}
        touched={element.config.touched}
        changed={event => this.inputChangeHandler(event, element.id)}
      />
    ));
    return (
      <div className={styles.AuthPage}>
        {authRedirect}
        <h1 className={styles.Title}>Manhattan</h1>
        <div className={styles.Box}>
          <input
            id={styles.signup}
            type="radio"
            name="form"
            value="signup"
            checked={this.state.selectedOption === "signup"}
            readOnly
          />
          <label
            htmlFor="signup"
            onClick={() => this.radioCheckedHandler("signup")}
          />
          <input
            id={styles.login}
            type="radio"
            name="form"
            value="login"
            checked={this.state.selectedOption === "login"}
            readOnly
          />
          <label
            htmlFor="login"
            onClick={() => this.radioCheckedHandler("login")}
          />
          <form className={styles.Form} onSubmit={this.onFormSubmitHandler}>
            <div className={styles.Content}>{form}</div>
            <button />
          </form>
        </div>
      </div>
    );
  }
}

const mapState = state => {
  return {
    error: state.auth.error,
    isAuth: state.auth.isAuth
  };
};

const mapActions = dispatch => {
  return {
    onAuth: (user, selectedOption) =>
      dispatch(actions.authentication(user, selectedOption))
  };
};

export default connect(mapState, mapActions)(Auth);
