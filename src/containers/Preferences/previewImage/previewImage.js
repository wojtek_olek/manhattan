import React from "react";
import styles from "./previewImage.scss";

const previewImage = props => {
  if (!props.imageUrl) {
    return <div className={styles.Picture}>{props.image}</div>;
  } else {
    return <img className={styles.Picture} src={props.imageUrl} alt="user" />;
  }
};

export default previewImage;
