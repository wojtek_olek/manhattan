import React, { Component } from "react";
import Page from "../../hoc/Page/Page";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Input from "../../components/UI/input/input";
import PreviewImage from "./previewImage/previewImage";
import Button from "../../components/UI/button/button";

// Redux
import * as actions from "../../store/actions/index";

class Preferences extends Component {
  state = {
    preferencesForm: {
      firstName: {
        elementType: "input",
        label: "First Name",
        elementConfig: {
          type: "text"
        },
        value:
          this.props.currentUser !== null
            ? this.props.currentUser.firstName
            : "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      lastName: {
        elementType: "input",
        label: "Last Name",
        elementConfig: {
          type: "text"
        },
        value:
          this.props.currentUser !== null
            ? this.props.currentUser.lastName
            : "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      phone: {
        elementType: "input",
        label: "Phone number",
        elementConfig: {
          type: "text"
        },
        value:
          this.props.currentUser !== null ? this.props.currentUser.phone : "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      city: {
        elementType: "input",
        label: "City",
        elementConfig: {
          type: "text"
        },
        value:
          this.props.currentUser !== null ? this.props.currentUser.city : "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      }
    },
    userPhoto: {
      elementType: "inputImage",
      label: "Change user photo:",
      elementConfig: {
        type: "file"
      }
    },
    userImage: {
      image: null,
      imageUrl:
        this.props.currentUser !== null ? this.props.currentUser.image : null
    }
  };

  checkValidity = (value, rules) => {
    let isValid = true;
    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    return isValid;
  };

  onImageChange = event => {
    let reader = new FileReader();
    let image = event.target.files[0];
    reader.onloadend = () => {
      this.setState({
        userImage: {
          image: image,
          imageUrl: reader.result
        }
      });
    };
    reader.readAsDataURL(image);
  };

  inputChangeHandler = (event, inputId) => {
    const upInputs = {
      ...this.state.preferencesForm,
      [inputId]: {
        ...this.state.preferencesForm[inputId],
        value: event.target.value,
        valid: this.checkValidity(
          event.target.value,
          this.state.preferencesForm[inputId].validation
        ),
        touched: true
      }
    };
    this.setState({ preferencesForm: upInputs });
  };

  submitPreferences = () => {
    const userData = {
      uId: this.props.uId,
      firstName: this.state.preferencesForm.firstName.value,
      lastName: this.state.preferencesForm.lastName.value,
      phone: this.state.preferencesForm.phone.value,
      city: this.state.preferencesForm.city.value,
      userPhoto: this.state.userImage.image,
      userPhotoUrl: this.state.userImage.imageUrl
    };
    this.props.updateUserData(userData);
  };

  render() {
    let authRedirect = null;
    if (!this.props.isAuth && !this.props.userLoading) {
      authRedirect = <Redirect to="/auth" />;
    }

    const formElements = [];
    for (let key in this.state.preferencesForm) {
      formElements.push({
        id: key,
        config: this.state.preferencesForm[key]
      });
    }
    const form = formElements.map(element => {
      return (
        <Input
          key={element.id}
          elementType={element.config.elementType}
          elementConfig={element.config.elementConfig}
          label={element.config.label}
          value={element.config.value}
          invalid={!element.config.valid}
          shouldValidate={element.config.validation}
          touched={element.config.touched}
          changed={event => this.inputChangeHandler(event, element.id)}
        />
      );
    });

    let page = null;
    if (this.props.currentUser !== null) {
      page = (
        <Page pageTitle="Preferences">
          {authRedirect}
          <div>
            {form}
            <Input
              elementType={this.state.userPhoto.elementType}
              elementConfig={this.state.userPhoto.elementConfig}
              label={this.state.userPhoto.label}
              value={this.state.userPhoto.value}
              changed={event => this.onImageChange(event)}
            />
            <PreviewImage
              image={this.state.userImage.image}
              imageUrl={this.state.userImage.imageUrl}
            />
          </div>
          <Button clicked={this.submitPreferences}>Save Preferences</Button>
        </Page>
      );
    }

    return <div>{page}</div>;
  }
}

const mapState = state => {
  return {
    isAuth: state.auth.isAuth,
    uId: state.auth.uId,
    currentUser: state.auth.currentUser,
    userLoading: state.auth.userLoading
  };
};

const mapActions = dispatch => {
  return {
    updateUserData: userData => dispatch(actions.updateUserData(userData)),
    getUserData: uId => dispatch(actions.getUser(uId))
  };
};

export default connect(mapState, mapActions)(Preferences);
