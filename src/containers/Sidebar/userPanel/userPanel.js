import React from "react";
import styles from "./userPanel.scss";
import { Link } from "react-router-dom";

import UserPhoto from "../../../assets/images/userPhoto.png";

const userPanel = props => {
  let userMenu = null;
  if (props.showMenu) {
    userMenu = (
      <div className={styles.UserMenu}>
        <span className={styles.UserName}>{props.userName}</span>
        <ul className={styles.Options}>
          <li className={styles.Option}><Link to="/preferences">Preferences</Link></li>
          <li className={styles.Option} onClick={props.logout}>
            Logout
          </li>
        </ul>
      </div>
    );
  }

  return (
    <div className={styles.UserPanel}>
      <img
        className={styles.UserImage}
        src={props.userPhoto ? props.userPhoto : UserPhoto}
        alt="user"
        onClick={props.toggleUserMenu}
      />
      {userMenu}
    </div>
  );
};

export default userPanel;
