import React, { Component } from "react";
import styles from "./Sidebar.scss";

// Panels
import NavItem from "./navItem/navItem";
import UserPanel from "./userPanel/userPanel";

import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

class Sidebar extends Component {
  constructor() {
    super();
    this.toggleUserMenuHandler = this.toggleUserMenuHandler.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
  }

  state = {
    userMenuOpen: false
  };

  sidebarRef = React.createRef();

  toggleUserMenuHandler() {
    if (!this.state.userMenuOpen) {
      document.addEventListener("click", this.handleOutsideClick, false);
    } else {
      document.removeEventListener("click", this.handleOutsideClick, false);
    }
    if (this.sidebarRef) {
      this.setState(prevState => ({
        userMenuOpen: !prevState.userMenuOpen
      }));
    }
  }

  handleOutsideClick(e) {
    this.toggleUserMenuHandler();
  }

  logoutHandler = () => {
    this.props.onLogout();
  };

  render() {
    const userPhoto =
      this.props.currentUser !== null ? this.props.currentUser.image : null;
    const userName =
      this.props.currentUser !== null
        ? this.props.currentUser.firstName +
          " " +
          this.props.currentUser.lastName
        : null;
    return (
      <div className={styles.Sidebar} ref={this.sidebarRef}>
        <UserPanel
          toggleUserMenu={this.toggleUserMenuHandler}
          showMenu={this.state.userMenuOpen}
          logout={this.logoutHandler}
          userPhoto={userPhoto}
          userName={userName}
        />
        <NavItem />
      </div>
    );
  }
}

const mapState = state => {
  return {
    currentUser: state.auth.currentUser
  };
};

const mapActions = dispatch => {
  return {
    onLogout: () => dispatch(actions.logout())
  };
};

export default connect(mapState, mapActions)(Sidebar);
