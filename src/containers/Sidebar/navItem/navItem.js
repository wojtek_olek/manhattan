import React from "react";
import { NavLink } from "react-router-dom";
import Aux from "../../../hoc/Aux/Aux";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { faBook, faHome } from "@fortawesome/fontawesome-free-solid";

const navItem = props => {
  return (
    <Aux>
      <NavLink to="/">
        <FontAwesomeIcon icon={faHome} />
      </NavLink>
      <NavLink to="/projects">
        <FontAwesomeIcon icon={faBook} />
      </NavLink>
    </Aux>
  );
};

export default navItem;
